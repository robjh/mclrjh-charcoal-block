local ref_coal_block = "mcl_core:coalblock"
local ref_charcoal = "mcl_core:charcoal_lump"
local ref_charcoal_block = "mclrjh_charcoal_block:charcoalblock"

-- copy the coal block's params, change it's description
local item_charcoal_block = {}
for k,v in pairs(minetest.registered_nodes[ref_coal_block]) do
	item_charcoal_block[k] = v
end
item_charcoal_block["description"] = "Block of Charcoal"
minetest.register_node(ref_charcoal_block, item_charcoal_block)

minetest.register_craft({
	output = ref_charcoal_block,
	recipe = {
		{ref_charcoal, ref_charcoal, ref_charcoal},
		{ref_charcoal, ref_charcoal, ref_charcoal},
		{ref_charcoal, ref_charcoal, ref_charcoal},
	},
})
minetest.register_craft({
	type = "shapeless",
	output = ref_charcoal .. " 9",
	recipe = {ref_charcoal_block}
})
minetest.register_craft({
	type = "fuel",
	recipe = ref_charcoal_block,
	burntime = 800.0
})
